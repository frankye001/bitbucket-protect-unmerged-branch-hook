package com.atlassian.bitbucket.plugin.hooks.protectbranch.repository;

import com.atlassian.bitbucket.repository.RefChange;
import com.atlassian.bitbucket.repository.RefChangeType;
import com.atlassian.bitbucket.repository.SimpleRefChange;

import static com.atlassian.bitbucket.repository.RefTestUtils.minimalBranch;

public class TestRefChangeBuilder {

    private final SimpleRefChange.Builder builder;

    public TestRefChangeBuilder() {
        builder = new SimpleRefChange.Builder();
        builder.ref(minimalBranch("branch-name")).fromHash("00000537").toHash("00000837").type(RefChangeType.UPDATE);
    }

    public TestRefChangeBuilder refId(String refId) {
        builder.ref(minimalBranch(refId));
        return this;
    }

    public TestRefChangeBuilder type(RefChangeType type) {
        builder.type(type);
        return this;
    }

    public TestRefChangeBuilder fromHash(String hash) {
        builder.fromHash(hash);
        return this;
    }

    public TestRefChangeBuilder toHash(String hash) {
        builder.toHash(hash);
        return this;
    }

    public RefChange build() {
        return builder.build();
    }
}
