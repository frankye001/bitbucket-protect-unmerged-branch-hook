package com.atlassian.bitbucket.plugin.hooks.protectbranch;

import com.atlassian.bitbucket.hook.repository.RepositoryHookContext;
import com.atlassian.bitbucket.i18n.I18nService;
import com.atlassian.bitbucket.i18n.SimpleI18nService;
import com.atlassian.bitbucket.nav.NavBuilder;
import com.atlassian.bitbucket.plugin.hooks.protectbranch.project.TestProjectBuilder;
import com.atlassian.bitbucket.plugin.hooks.protectbranch.pull.TestPullRequestBuilder;
import com.atlassian.bitbucket.plugin.hooks.protectbranch.util.TestPageUtils;
import com.atlassian.bitbucket.project.Project;
import com.atlassian.bitbucket.pull.*;
import com.atlassian.bitbucket.repository.RefChange;
import com.atlassian.bitbucket.repository.RefChangeType;
import com.atlassian.bitbucket.repository.Repository;
import com.atlassian.bitbucket.plugin.hooks.protectbranch.pull.TestPullRequestRefBuilder;
import com.atlassian.bitbucket.plugin.hooks.protectbranch.repository.TestRefChangeBuilder;
import com.atlassian.bitbucket.plugin.hooks.protectbranch.repository.TestRepositoryBuilder;
import com.atlassian.bitbucket.setting.Settings;
import com.atlassian.bitbucket.util.PageRequest;
import com.atlassian.bitbucket.util.PageUtils;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

import javax.annotation.Nonnull;
import java.util.Arrays;
import java.util.Collections;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.BDDMockito.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verifyNoMoreInteractions;

public class ProtectUnmergedBranchHookTest {

    public static final String TARGET_BRANCH = "target_branch";
    public static final String SOURCE_BRANCH = "feature/source_branch";
    public static final String RANDOM_BRANCH = "random_branch";

    public static final RefChange ADD_RANDOM_BRANCH = new TestRefChangeBuilder().refId(RANDOM_BRANCH).type(RefChangeType.ADD).build();
    public static final RefChange UPDATE_TARGET_BRANCH = new TestRefChangeBuilder().refId(TARGET_BRANCH).type(RefChangeType.UPDATE).build();
    public static final RefChange UPDATE_RANDOM_BRANCH = new TestRefChangeBuilder().refId(RANDOM_BRANCH).type(RefChangeType.UPDATE).build();
    public static final RefChange DELETE_TARGET_BRANCH = new TestRefChangeBuilder().refId(TARGET_BRANCH).type(RefChangeType.DELETE).build();
    public static final RefChange DELETE_SOURCE_BRANCH = new TestRefChangeBuilder().refId(SOURCE_BRANCH).type(RefChangeType.DELETE).build();
    public static final RefChange DELETE_RANDOM_BRANCH = new TestRefChangeBuilder().refId(RANDOM_BRANCH).type(RefChangeType.DELETE).build();

    private static final Project PROJECT = new TestProjectBuilder().key("PROJECT").build();
    private static final Repository REPOSITORY = new TestRepositoryBuilder().slug("REPO").project(PROJECT).build();
    private static final PullRequest TARGET_PULL_REQUEST = new TestPullRequestBuilder().id(1).state(PullRequestState.OPEN).toRef(new TestPullRequestRefBuilder().repository(REPOSITORY).build()).build();
    private static final PullRequest SOURCE_PULL_REQUEST = new TestPullRequestBuilder().id(2).state(PullRequestState.OPEN).toRef(new TestPullRequestRefBuilder().repository(REPOSITORY).build()).build();
    private static final Settings SETTINGS = mock(Settings.class);
    private static final RepositoryHookContext CONTEXT = new SimpleHookContext(REPOSITORY, SETTINGS);

    private final PullRequestService pullRequestService = mock(PullRequestService.class);
    private final NavBuilder navBuilder = mock(NavBuilder.class, RETURNS_DEEP_STUBS);
    private final I18nService i18nService = new SimpleI18nService(SimpleI18nService.Mode.FORMAT_MESSAGES);
    private final StubHookResponse hookResponse = new StubHookResponse();

    private final ProtectUnmergedBranchHook hook = new ProtectUnmergedBranchHook(pullRequestService, i18nService, navBuilder);

    @AfterClass
    public static void verifyNoInteractionsWithSettings() {
        verifyNoMoreInteractions(SETTINGS);
    }

    @Before
    public void setup() {
        // Return an empty page for any find in direction call - we'll override this if needed
        given(pullRequestService.search(any(PullRequestSearchRequest.class), any(PageRequest.class)))
                .willReturn(PageUtils.createEmptyPage(PageUtils.newRequest(0, 10)));

        // target branch is involved in PR1
        given(pullRequestService.search(refEq(new PullRequestSearchRequest.Builder()
                .repositoryAndBranch(PullRequestDirection.INCOMING, REPOSITORY.getId(), "refs/heads/" + TARGET_BRANCH)
                .state(PullRequestState.OPEN).build()), any(PageRequest.class)))
                .willReturn(TestPageUtils.pageContaining(TARGET_PULL_REQUEST));

        // source branch is involved in PR2
        given(pullRequestService.search(refEq(new PullRequestSearchRequest.Builder()
                .repositoryAndBranch(PullRequestDirection.OUTGOING, REPOSITORY.getId(), "refs/heads/" + SOURCE_BRANCH)
                .state(PullRequestState.OPEN).build()), any(PageRequest.class)))
                .willReturn(TestPageUtils.pageContaining(SOURCE_PULL_REQUEST));
    }

    @Test
    public void shouldRejectDeletionOfActivePullRequestTargetBranch() throws Exception {
        boolean receiveAllowed = hook.onReceive(CONTEXT, Collections.singletonList(DELETE_TARGET_BRANCH), hookResponse);

        assertFalse("Attempt to delete active pull request target branch should not be allowed", receiveAllowed);
    }

    @Test
    public void shouldRejectDeletionOfActivePullRequestSourceBranch() throws Exception {
        boolean receiveAllowed = hook.onReceive(CONTEXT, Collections.singletonList(DELETE_SOURCE_BRANCH), hookResponse);

        assertFalse("Attempt to delete active pull request source branch should not be allowed", receiveAllowed);
    }

    @Test
    public void shouldRejectDeletionOfActivePullRequestSourceAndTargetBranches() throws Exception {
        boolean receiveAllowed = hook.onReceive(CONTEXT, Arrays.asList(DELETE_TARGET_BRANCH, DELETE_SOURCE_BRANCH), hookResponse);

        assertFalse("Attempt to delete active pull request source and target branch should not be allowed", receiveAllowed);
    }

    @Test
    public void shouldAllowDeletionOfUnrelatedBranch() throws Exception {
        boolean receiveAllowed = hook.onReceive(CONTEXT, Collections.singletonList(DELETE_RANDOM_BRANCH), hookResponse);

        assertTrue("Deleting branches is allowed if they are not connected to active pull requests", receiveAllowed);
    }

    @Test
    public void shouldRejectAnActiveBranchDeleteInterspersedWithOtherChanges() throws Exception {
        boolean receiveAllowed = hook.onReceive(CONTEXT, Arrays.asList(ADD_RANDOM_BRANCH, UPDATE_TARGET_BRANCH, DELETE_TARGET_BRANCH, UPDATE_RANDOM_BRANCH, DELETE_RANDOM_BRANCH), hookResponse);

        assertFalse("Attempt to delete active pull request target branch should not be allowed even if other, uninteresting changes are included", receiveAllowed);
    }

    @Test
    public void shouldAllowChangesToActiveBranch() throws Exception {
        boolean receiveAllowed = hook.onReceive(CONTEXT, Collections.singletonList(UPDATE_TARGET_BRANCH), hookResponse);

        assertTrue("Changes to branches with active pull requests are allowed", receiveAllowed);
    }

    @Test
    public void shouldListAllBranchesAndActivePullRequestsOnErr() throws Exception {
        given(navBuilder.repo(REPOSITORY).pullRequest(1).buildAbsolute()).willReturn("http://url.to/project/repo/1");
        given(navBuilder.repo(REPOSITORY).pullRequest(2).buildAbsolute()).willReturn("http://url.to/project/repo/2");

        hook.onReceive(CONTEXT, Arrays.asList(DELETE_TARGET_BRANCH, DELETE_SOURCE_BRANCH), hookResponse);

        // also tests that we strip refs/heads if it exists (on source branch) but don't freak out if it doesn't (on target branch)
        hookResponse.assertStdErr(allOf(containsString("'target_branch'"),
                                        containsString("http://url.to/project/repo/1"),
                                        containsString("'feature/source_branch'"),
                                        containsString("http://url.to/project/repo/2")));
        hookResponse.assertStdOut(is(""));
    }

    @Test
    public void shouldNotSendAnyOutputForAllowedChanges() throws Exception {
        hook.onReceive(CONTEXT, Arrays.asList(ADD_RANDOM_BRANCH, UPDATE_TARGET_BRANCH, UPDATE_RANDOM_BRANCH, DELETE_RANDOM_BRANCH), hookResponse);

        hookResponse.assertStdOut(is(""));
        hookResponse.assertStdErr(is(""));
    }

    private static class SimpleHookContext extends RepositoryHookContext {
        public SimpleHookContext(@Nonnull Repository repository, @Nonnull Settings settings) {
            super(repository, settings);
        }
    }
}
