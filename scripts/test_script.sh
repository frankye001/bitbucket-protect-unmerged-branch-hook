#!/bin/bash

set -e

echo
echo Clone repo
git clone http://admin:admin@localhost:7990/bitbucket/scm/PROJECT_1/rep_1.git
cd rep_1

echo
echo Make target branch
git checkout -b target_branch
echo target branch >> target.txt
git add target.txt
git commit -m "adding target branch"
git push -u origin target_branch

echo
echo Make source branch 1
git checkout -b source_1_branch
echo source 1 >> target.txt
git commit -a -m "adding source 1 branch"
git push -u origin source_1_branch

echo
echo Make source branch 2
git checkout target_branch
git checkout -b source_2_branch
echo source 2 >> target.txt
git commit -a -m "adding source 2 branch"
git push -u origin source_2_branch

echo
echo Pause to allow time to enable hook and set up pull requests...
read

set +e

echo
echo Try to delete source 1 branch...
git push origin :source_1_branch

echo
echo "Try to delete target branch (should show 2 errors)"
git push origin :target_branch

echo Pause to allow time to decline source_1 pull request...
read

echo
echo Delete source 1 branch to show branches can be deleted after they are closed...
git push origin :source_1_branch

echo
echo "Pause to allow time to decline pull requests for cleanup..."
read

echo
echo Cleaning up
git checkout master
git push origin :source_2_branch
git push origin :target_branch
cd ..
rm -rf rep_1
